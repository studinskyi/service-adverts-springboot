package adverts.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import adverts.entities.Advertisment;

@Repository
public interface AdvertismentRepository extends CrudRepository<Advertisment, Long> {

}
